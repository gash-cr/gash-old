module.exports = function(config) {
  config.set({

    basePath: '',
    frameworks: ['jasmine'],

    files: [
      './www/lib/angular/angular.js',
      './www/lib/angular-ui-router/release/angular-ui-router.js',
      './www/lib/angular-animate/angular-animate.js',
      './www/lib/angular-cookies/angular-cookies.js',
      './www/lib/angular-messages/angular-messages.js',
      './www/lib/angular-sanitize/angular-sanitize.js',
      './www/lib/angular-touch/angular-touch.js',
      './www/lib/angular-bootstrap/ui-bootstrap-tpls.js',
      './www/lib/angular-bootstrap/ui-bootstrap-tpls.js',
      './www/lib/firebase/firebase.js',
      './www/lib/angularfire/dist/angularfire.js',
      './www/lib/angular-toastr/dist/angular-toastr.tpls.js',
      './www/lib/angular-mocks/angular-mocks.js',
      './www/app/components/**/*.module.js',
      './www/app/components/**/*.js',
      './www/app/components/*.js',
      '.www/tests/specs/**/*.spec.js'
    ],

    exclude: [],

    preprocessors: {
        './www/app/components/**/*.js': ['coverage']
    },

    coverageReporter: {
      type : 'html',
      dir : 'coverage/',
    },


    reporters: ['progress', 'coverage'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['PhantomJS'],
    singleRun: true,
    concurrency: Infinity
  })
};
