(function() {
  'use strict';

  angular
    .module('app.core')
    .service('LoginService', LoginService);

  /* @ngInject */

  function LoginService(SessionService, $rootScope, $location, $state, $firebaseObject,$http, CORE) {

    var service = {
      rootRef: firebase.database().ref(),
      api_url: CORE.API_URL,
      isLoggedIn: isLoggedIn,
      logIn: logIn,
      logOut: logOut,
      verifyAccess: verifyAccess
    };

    return service;

    function logIn(username) {
      var userRef = service.rootRef.child('users').child(username);
      return $firebaseObject(userRef).$loaded();
    }

    function isLoggedIn() {
      var authData = SessionService.getAuthData();
      var sessionDefined = typeof authData !== 'undefined';
      var authDataDefined = authData !== null;
      return sessionDefined && authDataDefined;
    }

    function logOut() {
      SessionService.destroy();
      $state.go('login');
    }

    function verifyAccess() {
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
        if (!service.isLoggedIn() && toState.name !== 'login') {
          $location.path('login');
        }
      });
    }

    function redirect() {
      if(service.redirectInfo) {
        $state.go(service.redirectInfo.toStateName, service.redirectInfo.toParams);
      } else {
        $state.go('mainMenu');
      }
    }

  }
})();
