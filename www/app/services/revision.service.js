(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('RevisionService', RevisionService);

  /* @ngInject */
  function RevisionService($firebaseObject, 
                           $firebaseArray, 
                           $http,
                           CORE,
                           DamagesService) {

    var service = {
      rootRef: firebase.database().ref(),
      url: CORE.API_URL,
      container: {},
      revision: {},
      setContainer: setContainer,
      resetContainer: resetContainer,
      getContainer: getContainer,
      getRevision: getRevision,
      getRevisions: getRevisions,
      resetRevision: resetRevision,
      setRemarkInfo: setRemarkInfo,
      pushRevision: pushRevision,
      getRevisionRef: getRevisionRef,
      pushDamages: pushDamages,
      uploadPhotos: uploadPhotos
    };

    return service;

    function setContainer(container) {
      service.container = container;
    }

    function getContainer() {
      return service.container;
    }

    function getRevision(revNumber) {
      var revisionRef = service.rootRef.child('revisions').child(revNumber);
      return $firebaseObject(revisionRef).$loaded();
    }

    function getRevisions() {
      var revisionRef = service.rootRef.child('revisions');
      return $firebaseArray(revisionRef).$loaded();
    }

    function resetContainer() {
      service.container = {};
    }

    function resetRevision() {
      service.revision = {};
    }

    function setRemarkInfo(remark){
      service.revision.remark = remark;
    }

    function pushRevision(revisionData){
      var revisionsRef = service.rootRef.child('revisions');
      revisionsRef.push(revisionData);
    }

    function pushDamages(damageData) {
      var revisionRef= service.rootRef.child('damages');
      return revisionRef.push(damageData);
    }

    function getRevisionRef(time) {
      var RevisionRef = service.rootRef.child('revisions').orderByChild('timestamp').equalTo(time);
      return $firebaseArray(RevisionRef).$loaded()
    }

    function uploadPhotos() {
      for (var revisionDamages in DamagesService.damage){
        for (var damage in DamagesService.damage[revisionDamages]){
          uploadFiles(damage.image_ref);
        }
      }
    }

    function uploadFiles(address) {
      var storageRef = firebase.storage().ref();
      var damageRef = storageRef.child('address');
      var file = new File(address);

      var uploadTask = storageRef.child('images/' + file.name).put(file);

      uploadTask.on('state_changed', function(snapshot){
      }, function() {
        var downloadURL = uploadTask.snapshot.downloadURL;
      });
    }
  }
})();
