(function () {
  'use strict';

  angular
    .module('app.core')
    .factory('IonicClosePopupService', IonicClosePopupService);
  
  /* ngInject */
  function IonicClosePopupService() {
    
    var service = {
      currentPopup: [],
      initIonicService: initIonicService,
      register: register
    }

    return service;

    function initIonicService() {
      var htmlEl = angular.element(document.querySelector('html'));
      htmlEl.on('click', onClosePopUpOutsideClick)
    }

    function onClosePopUpOutsideClick(event) {
      if (event.target.nodeName === 'ION-VIEW' || event.target.nodeName === 'HTML' 
      || event.target.nodeName === 'A') {
        if (service.currentPopup) {
          angular.forEach(service.currentPopup, function (popUp) {
            popUp.close();
          });
        }
      }
    }

    function register(popup) {
      service.currentPopup.push(popup);
    }
  }

})();
