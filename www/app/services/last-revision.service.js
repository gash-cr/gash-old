(function() {
  'use strict';

  angular
  .module('app.core')
  .factory('LastRevisionService', LastRevisionService);

  /* @ngInject */
  function LastRevisionService($firebaseObject) {

    var rootRef = firebase.database().ref();
    var service = {
      lastRevisionId: '',
      getDamages: getDamages
    };

    return service;

    function getDamages(lastDamagesRef) {
      var damagesRef = rootRef.child('damages').child(lastDamagesRef);
      return $firebaseObject(damagesRef).$loaded();
    }
  }
})();
