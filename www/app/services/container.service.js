(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('ContainerService', ContainerService);

  /* @ngInject */
  function ContainerService($firebaseObject, $firebaseArray) {
    var service = {
      rootRef: firebase.database().ref(),
      getContainers: getContainers,
      getContainer: getContainer,
      pushContainer: pushContainer,
      updateContainer: updateContainer
    };

    return service;

    function getContainer(containerNumber) {
      var containerRef = service.rootRef.child('containers').child(containerNumber);
      return $firebaseObject(containerRef).$loaded();
    }

    function getContainers() {
      var containerRef = service.rootRef.child('containers');
      return $firebaseArray(containerRef).$loaded();
    }

    function pushContainer(containerData){
      var containerRef = service.rootRef.child('containers').child(containerData.number);
      containerRef.set(containerData);
    }

    function updateContainer(containerData) {
      containerData.$save();
    }
  }
})();
