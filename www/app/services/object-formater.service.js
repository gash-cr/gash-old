(function() {
  'use strict';

  angular
  .module('app.core')
  .service('ObjectFormaterService', ObjectFormaterService);

  /* @ngInject */
  function ObjectFormaterService(UNUSED_PROPERTIES, $q) {

    var service = {
      formatDamages: formatDamages
    };

    return service;

    function formatDamages(damagesList) {
      var damagesFormatDeferred = $q.defer();
      angular.forEach(UNUSED_PROPERTIES, function(PROPERTY) {
        delete damagesList[PROPERTY];
      });
      damagesFormatDeferred.resolve(damagesList);
      return damagesFormatDeferred.promise;
    }
  }
})();
