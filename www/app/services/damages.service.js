(function() {
  'use strict';

  angular
  .module('app.core')
  .factory('DamagesService', DamagesService);

  /* @ngInject */
  function DamagesService($firebaseObject,
                          $firebaseArray,
                          $http,
                          $rootScope,
                          $q) {

    var storageRef = firebase.storage().ref();
    var service = {
      rootRef: firebase.database().ref(),
      getContainerCompany: getContainerCompany,
      clearCurrentDamages: clearCurrentDamages,
      loadPreviousDamage : loadPreviousDamage,
      addTemporaryDamage: addTemporaryDamage,
      getActualDamages: getActualDamages,
      getDamage: getDamage,
      setActualView: setActualView,
      uploadPhotos: uploadPhotos,
      getImgPath: getImgPath,
      saveDamages: saveDamages,
      setPhoto : setPhoto,
      replaceID:replaceID,
      actualContainer: {},
      currentDamages: [],
      savedDamages: [],
      photoExist: false,
      photo: null
    };

    return service;

    function setActualView(container_view, shipping_co) {
      service.actualContainer.view = container_view;
      service.actualContainer.company = shipping_co;
    }

    function getContainerCompany() {
      return service.actualContainer.company;
    }

    function getDamage(id) {
      var damageRef = service.rootRef.child('damages').child(id);
      return $firebaseObject(damageRef).$loaded();
    }

    function getImgPath() {
      return "img\/" + service.actualContainer.company + "\/" + service.actualContainer.view + ".png";
    }

    function clearCurrentDamages() {
      service.currentDamages = [];
    }

    function loadPreviousDamage() {
      for (var view in service.savedDamages){
        if (view === service.actualContainer.view){
          service.currentDamages = angular.copy(service.savedDamages[view]);
          break;
        }
      }
      return service.currentDamages;
    }

    function getActualDamages() {
      return service.savedDamages[service.actualContainer.view];
    }

    function setPhoto(image) {
      service.photo = image;
    }

    function addTemporaryDamage(damage){
      if(service.photo) {
        damage.img_ref = service.photo;
        service.photo = null;
        service.photoExist = false;
      }
      service.currentDamages.push(damage);
    }

    function saveDamages() {
      var formattedDamages = angular.fromJson(angular.toJson(service.currentDamages));
      service.savedDamages[service.actualContainer.view] = [];
      angular.forEach(formattedDamages, function (damage) {
        service.savedDamages[service.actualContainer.view].push(damage);
      });
      clearCurrentDamages();
    }

    function uploadPhotos() {
      var damageList = service.savedDamages;
      for (var view in damageList){
        for (var damage in damageList[view]){
          if (angular.isDefined(damageList[view][damage].img_ref)) {
            uploadFiles(damageList[view][damage].img_ref)
              .then(function(imgRef) {
                service.savedDamages[view][damage].img_ref = imgRef;
              });
          }
        }
      }
    }

    function uploadFiles(base64Damage) {
      var imageUploadDefer = $q.defer(); 
      var timestamp = Date.now();
      var refName = 'damage' + timestamp + '.jpg';
      var photoStorageRef = storageRef.child(refName);
      photoStorageRef.putString(base64Damage, 'base64')
        .then(function(snapshot) {
          $log.info(snapshot);
          imageUploadDefer.resolve(refName);
        })
        .catch(function(error) {
          $log.error(error);
          imageUploadDefer.resolve(reject);
        });
      return imageUploadDefer.promise;
    }

    function replaceID(oldDamageId,newDamageID) {
      angular.forEach(service.currentDamages,function (damage) {
        if (damage.id == oldDamageId){
          var index = service.currentDamages.indexOf(damage);
          damage.id = newDamageID;
          service.currentDamages[index] = damage;
        }
      });
    }
  }
})();
