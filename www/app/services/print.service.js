(function() {
  'use strict';

  angular
    .module('app.core')
    .service('PrintService', PrintService);

    /* @ngInject */
  function PrintService($ionicPopup, $q) {

    var printDeffer = $q.defer();
    var service = {
      printReport: printReport
    };

    return service;

    function printReport(revision, address) {
      var ADDRESS = 'http://' + address + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=60000';
      var epos = new epson.ePOSPrint(ADDRESS);
      var builder = buildMessage(revision);

      epos.onreceive = function (res) {
        $ionicPopup.alert({
          template: 'Agregado a la cola de impresión',
          title: 'Aviso!',
          cssClass: 'text-center'
        });
        printDeffer.reject();
      };

      epos.onerror = function (err) {
        $ionicPopup.alert({
          template: 'Impresora no conectada',
          title: 'Aviso!',
          cssClass: 'text-center'
        });
        printDeffer.reject();
      };

      epos.oncoveropen = function () {
        $ionicPopup.alert({
          template: 'Impresora abierta',
          title: 'Aviso!',
          cssClass: 'text-center'
        });
        printDeffer.reject();
      };

      epos.send(builder.toString());
      return printDeffer.promise;
    }

    function buildMessage(revision) {
      var builder = new epson.ePOSBuilder();
      var shippingContainer = revision.shipping_company ? revision.shipping_company : revision.shipping_co;
      var canvas = document.getElementById('canvas-report');
      var context = canvas.getContext('2d');
      var logo = document.getElementById('report-logo');
      context.drawImage(logo, 10, 10);
      builder.addPageBegin(); 
      builder.addPageArea(0, 0, 300, 300);
      builder.addPagePosition(0, 299);
      builder.addImage(context, 0, 0, canvas.width, canvas.height); 
      builder.addPageEnd();
      builder.addFeedLine(2);
      builder.addTextLang('en');
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Fecha y Hora: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((dateFormatter(new Date(revision.timestamp)) || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Entrada y Salida: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.container_status || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Contenedor: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.container || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Tipo de Contenedor:');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.container_type || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Marchamo 1: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.seal_no_1 || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Marchamo 2: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.seal_no_2 || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Marchamo 3: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.seal_no_3 || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Marchamo 4: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.seal_no_4 || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Marchamo 5: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.seal_no_5 || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Marchamo 6: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.seal_no_6 || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Naviera: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((shippingContainer || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Medida: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.size || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Predio Origen: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.origin || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Guia: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.tracking_id || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Predio Destino: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.destination || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Cabezal: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.carrier_no || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Conductor: ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.driver || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Chasis:');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.chassis_no || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Tipo de Chasis:');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.chassis_type || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Observaciones:').addFeed();
      printObservations(builder, revision);
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('M D #1/2 ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.tire_seal_1 || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('M D #3/4 ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.tire_seal_2 || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('M D #5/6 ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.tire_seal_3 || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('M I #1/2 ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.tire_seal_4 || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('M I #3/4 ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.tire_seal_5 || '')).addFeed();
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('M I #5/6 ');
      builder.addTextStyle(false, false, false, builder.COLOR_1);
      builder.addText((revision.tire_seal_6 || '')).addFeed();
      printParts(builder, revision);
      builder.addFeedLine(2);
      builder.addTextStyle(false, false, true, builder.COLOR_1);
      builder.addText('Daños ').addFeed();
      printDamages(builder, revision);
      builder.addCut(builder.CUT_FEED);
      return builder;
    }

    function printObservations(builder, revision) {
      if(revision.remarks) {
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        builder.addText(revision.remarks || '');
        builder.addFeedLine(2);
      } else {
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addText('Sin Observaciones ').addFeed();
      }
    }

    function printDamages(builder, revision) {
      if(revision.damages.length > 0) {
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        angular.forEach(revision.damages, function(damage) {
          builder.addText('Parte: ' + (damage.part + ', Daño: ' + damage.name || '')).addFeed();
          builder.addTextStyle(false, false, false, builder.COLOR_1);
        });
      } else {
          builder.addTextStyle(false, false, true, builder.COLOR_1);
          builder.addText('Sin Daños ').addFeed();
      }
    }

    function printParts(builder, revision) {
      if(angular.isDefined(revision.parts)) {
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addText('Parte   ');
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addText('Entrada   ');
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addText('Salida   ').addFeed();
        if(revision.parts.length > 0) {
          builder.addTextStyle(false, false, false, builder.COLOR_1);
          angular.forEach(revision.parts, function(part) {
            var partIn = part.in ? 'si' : 'no';
            var partOut = part.out ? 'si' : 'no';
            builder.addText(part.name + '  ' + partIn + '  ' + partOut).addFeed();
            builder.addTextStyle(false, false, false, builder.COLOR_1);
          });
        } else {
            builder.addTextStyle(false, false, true, builder.COLOR_1);
            builder.addText('Sin Partes E/S ').addFeed();
        }
      }
    }

    function dateFormatter(givenDate) {
      return (givenDate.getDate() + '/' + (givenDate.getMonth() + 1) +
       '/' + givenDate.getFullYear() + ' ' + givenDate.getHours()
        + ':' + givenDate.getMinutes());
    }
  }

} ());
