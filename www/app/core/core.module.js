(function() {
  'use strict';

  /* @ngInject */
  function initAuth(LoginService, $rootScope) {
    $rootScope.logOut = LoginService.logOut;
    $rootScope.isLoggedIn = LoginService.isLoggedIn;
    LoginService.verifyAccess();
  }

  angular
    .module('app.core', [
      'ionic',
      'ui.router',
      'firebase',
      'ngMessages',
      'ngCordova'
    ])
    .run(initAuth);

})();
