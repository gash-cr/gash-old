(function() {
  'use strict';

  var BASE_PATH = 'https://gash-db-api-staging.herokuapp.com/';
  var TESTING_PATH = 'http://localhost:8080/api/v1/';

  angular
  .module('app')
  .constant('CORE', {
    'API_URL': BASE_PATH
  })
  .constant('_', window._)
  .constant('GASH_DAMAGES', [
    {'abbreviation': 'C', 'name': 'Cortadura'},
    {'abbreviation': 'R', 'name': 'Rayón'},
    {'abbreviation': 'H', 'name': 'Hueco'},
    {'abbreviation': 'A', 'name': 'Abolladura'},
    {'abbreviation': 'Q', 'name': 'Quebradura'},
    {'abbreviation': 'F', 'name': 'Faltante'},
    {'abbreviation': 'P', 'name': 'Parche'},
    {'abbreviation': 'G', 'name': 'Gotera'}
  ])
  .constant('NYK_DAMAGES', [
    {'abbreviation': 'C', 'name': 'Cortadura'},
    {'abbreviation': 'B', 'name': 'Bruise'},
    {'abbreviation': 'H', 'name': 'Hueco'},
    {'abbreviation': 'D', 'name': 'Abolladura'},
    {'abbreviation': 'BR', 'name': 'Quebradura'},
    {'abbreviation': 'M', 'name': 'Faltante'}
  ])
  .constant('EVERGREEN_DAMAGES', [
    {'abbreviation': 'C', 'name': 'Cortadura'},
    {'abbreviation': 'SC', 'name': 'Rayón'},
    {'abbreviation': 'H', 'name': 'Hueco'},
    {'abbreviation': 'D', 'name': 'Abolladura'},
    {'abbreviation': 'BR', 'name': 'Quebradura'},
    {'abbreviation': 'M', 'name': 'Faltante'},
    {'abbreviation': 'B', 'name': 'Pandeo'},
    {'abbreviation': 'BN', 'name': 'Doblado'},
    {'abbreviation': 'CO', 'name': 'Corrosión'},
    {'abbreviation': 'CR', 'name': 'Grieta'},
    {'abbreviation': 'S', 'name': 'Manchado'}
  ])
  .constant('TIR_PARTS', [
    {'name': 'Pulmones', 'in': false, 'out': false},
    {'name': 'Twist Locks', 'in': false, 'out': false},
    {'name': 'Manguera', 'in': false, 'out': false},
    {'name': 'Manilla', 'in': false, 'out': false},
    {'name': 'Faldones', 'in': false, 'out': false},
    {'name': 'Alambrados', 'in': false, 'out': false},
    {'name': 'Patas de Apoyo', 'in': false, 'out': false},
    {'name': 'Cintas Reflectoras', 'in': false, 'out': false},
    {'name': 'Cadena', 'in': false, 'out': false},
    {'name': 'Resortes, etc', 'in': false, 'out': false},
    {'name': 'Direccionales', 'in': false, 'out': false},
    {'name': 'Stop', 'in': false, 'out': false},
    {'name': 'Cola', 'in': false, 'out': false},
    {'name': 'Luces Laterales', 'in': false, 'out': false},
    {'name': 'Otros', 'in': false, 'out': false},
    {'name': 'Cigüeñal', 'in': false, 'out': false}
  ])
  .constant('DAMAGE_PARTS',[
    {'hasLeft':false},
    {'hasRight':false},
    {'hasBack':false},
    {'hasFront':false},
    {'hasChasisBack':false},
    {'hasChasisFront':false},
    {'hasChasisSide':false},
    {'hasChasisUp':false},
    {'hasInside':false},
    {'hasRoof':false},
    {'hasFloor':false}
  ])
  .constant('UNUSED_PROPERTIES', ['$$conf', '$priority', '$value', '$id']);
})();
