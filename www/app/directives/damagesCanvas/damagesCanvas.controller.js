(function() {
  'use strict';

  angular
  .module('app')
  .controller('DamagesCanvasController', DamagesCanvasController);

  /* @ngInject */
  function DamagesCanvasController($ionicPopup,
                                   $scope,
                                   GASH_DAMAGES,
                                   NYK_DAMAGES,
                                   EVERGREEN_DAMAGES,
                                   DamagesService,
                                   $cordovaCamera,
                                   IonicClosePopupService) {

    var vm = $scope;
    vm.changePhotoStatus = changePhotoStatus;
    vm.getPhotoStatus = getPhotoStatus;
    vm.takePicture = takePicture;
    vm.deleteShape = deleteShape;
    vm.loadDamage = loadDamage;
    vm.downEvent = downEvent;
    vm.addDamage = addDamage;
    activate();

    function activate() {
      IonicClosePopupService.initIonicService();
      initializeValues();
      initPaperLibrary();
      setCanvasBackground();
      loadDamage();
    }

    function initializeValues() {
      var company = DamagesService.getContainerCompany();
      var damages = [];
      switch (company) {
        case 'gash':
          damages = GASH_DAMAGES;
          break;
        case 'nyk':
          damages = NYK_DAMAGES;
          break;
        case 'evergreen':
          damages = EVERGREEN_DAMAGES;
          break;
      }
      $scope.damageModel = {
        value: damages[0].abbreviation,
        damagesList: damages
      };
      vm.photoExists  = false;
      vm.damages      = [];
      vm.shape        = null;
    }

    function initPaperLibrary() {
      paper.install(window);
      paper.setup('canvas');
    }

    function downEvent(event) {
      showDialog(true, event);
    }

    function showDialog(fromCanvas, event) {
      var confirmPopUp = $ionicPopup.confirm({
        title: 'Agregar daño',
        templateUrl: 'app/directives/damagesCanvas/addDamage.html',
        scope: $scope,
        buttons: [{
          text: '<i class="icon ion-close-round"></i>'
        },
        {
          text: '<i class="icon ion-checkmark-round"></i>',
          onTap: function (e) {
            if (e) {
              drawShape(event);
              addDamage(event);
            }
          }
        }]
      });
      IonicClosePopupService.register(confirmPopUp);
    }

    function drawShape(event) {
      drawDamage(event);
      paper.view.update();
      setXandYPercentages(event.point);
    }

    function addDamage(event) {
      setXandYPercentages(event.point);
      var name = findDamageName();
      var damage = {
        'id': vm.shape.id,
        'name': name,
        'abbreviation': $scope.damageModel.value,
        'relative_cords': {
          'x_percentage': vm.percentages.x_percentage,
          'y_percentage': vm.percentages.y_percentage
        },
        'imageRef':vm.imgURI
      };
      DamagesService.addTemporaryDamage(damage);
    }

    function findDamageName(){
      var damageObj = _.find($scope.damageModel.damagesList, { 'abbreviation': $scope.damageModel.value});
      return damageObj.name;
    }

    function setXandYPercentages(point) {
      vm.percentages = {
        x_percentage: (point.x / paper.view.size.width),
        y_percentage: (point.y / paper.view.size.height)
      };
    }

    function drawDamage(event) {
      event.point = getPoint(event);
      vm.shape = new PointText(event.point);
      vm.shape.justification = 'center';
      vm.shape.content = $scope.damageModel.value;
      vm.shape.fillColor = '#ED5505';
      vm.shape.fontSize = 25;
    }

    function getPoint(event) {
      var canvas = document.getElementById('canvas');
      var fixedX = event.x;
      var fixedY = event.y + 2;
      fixedX -= canvas.offsetLeft;
      fixedY -= canvas.offsetTop;
      return new Point(fixedX, fixedY);
    }

    function scaleImage(raster) {
      var heightScale = (paper.view.size.height / raster.height);
      var widthScale = (paper.view.size.width / raster.width);

      raster.scale(widthScale, heightScale);
    }

    function deleteShape(shapeId) {
      var canvasElement = _.find(project._activeLayer.children, { '_id': shapeId});
      canvasElement.remove();
      paper.view.update();
    }

    function setCanvasBackground() {
      var raster = null;
      var canvasURL = DamagesService.getImgPath();

      raster = new paper.Raster({
        source: canvasURL,
        position: paper.view.center
      });

      raster.onLoad = function () {
        scaleImage(raster);
      };
    }

    $scope.$on('damageDeleted', function (event, damage) {
      vm.deleteShape(damage.id);

    });

    function takePicture() {
      var options = {
        quality : 70,
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : Camera.PictureSourceType.CAMERA,
        allowEdit : false,
        encodingType: Camera.EncodingType.JPEG,

        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options)
        .then(function(imageData) {
          var base64Image = "data:image/jpeg;base64," + imageData;
          DamagesService.setPhoto(imageData);
          $scope.picture = base64Image;
          DamagesService.photoExist = true;
        },function(err) {
          $ionicPopup.alert({
            title: 'Error',
            template: 'Algo sucedió, vuelva a intentarlo'
          })
        })
    }

    function getPhotoStatus() {
      return DamagesService.photoExist;
    }

    function changePhotoStatus() {
      DamagesService.photoExist = false;
    }

    function loadDamage() {
      var actualDamages = DamagesService.getActualDamages();
      angular.forEach(actualDamages, function(damage) {
        addDamageSymbol(damage.relative_cords.x_percentage, damage.relative_cords.y_percentage, damage.abbreviation);
        DamagesService.replaceID(damage.id,vm.shape.id);
        damage.id = vm.shape.id;
      });
    }

    function addDamageSymbol(xValue, yValue, abbreviation) {
      var xPosition = (parseFloat(xValue) * (paper.view.size.width));
      var yPosition = (parseFloat(yValue) * (paper.view.size.height));
      vm.shape = new PointText(new Point(xPosition,yPosition));
      vm.shape.justification = 'center';
      vm.shape.content = abbreviation;
      vm.shape.fillColor = '#ED5505';
      vm.shape.fontSize = 25;
      paper.view.update();
    }
  }
})();
