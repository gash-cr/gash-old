(function() {

  angular
    .module('app')
    .directive('damagesCanvas', damagesCanvas);

  function damagesCanvas() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directives/damagesCanvas/damagesCanvas.html',
      controller: 'DamagesCanvasController as vm',
      scope: true
    };

    return directive;
  }

})();
