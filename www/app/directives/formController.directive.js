angular
  .module('app')
  .directive('ngDynamicController', ngDynamicController);

  function ngDynamicController ($compile) {
    return {
      parameters:{
        key :'='
      },
      restrict: 'E',
      link: function(scope,element,parameters) {

        var template='';
        switch(parameters.key) {
          case 'gash':
            template = '<div ng-include="\'app/revision/gash/gashForm.html\'"></div>';
            break;
          case 'nyk':
            template = '<div ng-include="\'app/revision/nyk/nykForm.html\'"></div>';
            break;
          case 'evergreen':
            template = '<div ng-include="\'app/revision/evergreen/evergreenForm.html\'"></div>';
            break;
        }
        var finalTemplate = $compile(template)(scope);
        element.append(finalTemplate);
      }
    };
  };
