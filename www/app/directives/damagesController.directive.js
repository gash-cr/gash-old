angular
  .module('app')
  .directive('ngDynamicControllerDamages',ngDynamicControllerDamages);

  function ngDynamicControllerDamages($compile) {
    return {
      parameters:{
        key :'='
      },
      restrict: 'E',
      link: function(scope,element,parameters) {

        var template='';

        switch(parameters.key) {
          case 'gash':
            template = '<div ng-include="\'app/revision/gash/gashDamages.html\'"></div>';
            break;
          case 'nyk':
            template = '<div ng-include="\'app/revision/nyk/nykDamages.html\'"></div>';
            break;
          case 'evergreen':
            template = '<div ng-include="\'app/revision/evergreen/evergreenDamages.html\'"></div>';
            break;
        }
        var finalTemplate = $compile(template)(scope);
        element.append(finalTemplate);
      }
    };
  };
