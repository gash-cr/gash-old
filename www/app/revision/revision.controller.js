(function() {
  'use strict';

  angular
  .module('app.revision')
  .controller('RevisionController', RevisionController);

  /* @ngInject */
  function RevisionController($scope,
                              $ionicModal,
                              $q,
                              $state,
                              $ionicPopup,
                              $ionicLoading,
                              $cordovaPrinter,
                              RevisionService,
                              ContainerService,
                              SessionService,
                              DamagesService,
                              TIR_PARTS,
                              PARTS) {
    var vm = this;
    var damageId = null;
    var revisionId = null;
    var lastRevisiondDeferred = $q.defer();
    var timestamp = Date.now();

    vm.goToViewDamages = goToViewDamages;
    vm.goToPreview = goToPreview;
    vm.hasNewDamages = hasNewDamages;
    vm.deleteDamage = deleteDamage;
    vm.saveRevision = saveRevision;
    vm.toggleAll = toggleAll;
    vm.noGenSet = noGenSet;
    vm.clearMoveReason = clearMoveReason;
    vm.save = save;
    vm.exit = exit;
    vm.sort = {
      property: 'name',
      reverse: false
    };
    vm.sortBy = sortBy;

    activate();

    function activate() {
      vm.currentDamages = DamagesService.currentDamages;
      vm.container = RevisionService.getContainer();
      vm.tir_parts = TIR_PARTS;
      vm.allOutPartsChecked = false;
      vm.allInPartsChecked = false;
      vm.currentRevision = {};
      vm.revision = {};
      setDefaultValues();
    }

    function sortBy(propertyName) {
      vm.sort.reverse = (vm.sort.property === propertyName) ? !vm.sort.reverse : false;
      vm.sort.property = propertyName;
    }

    function setDefaultValues() {
      vm.revision.origin = 'Caldera';
      vm.revision.destination = 'Limón'
      vm.revision.move_reason = 'export';
      vm.revision.container_status = 'Vacío';
      vm.revision.container_type = 'Estandar (ST)';
      if(vm.container.shipping_co === 'evergreen' || vm.container.shipping_co === 'nky') {
        vm.revision.size = '20';
      }
      vm.revision.tire_condition = 'Nuevas';
      vm.revision.chassis_type = 'GASH';
      vm.revision.gen_set = false;
    }

    function fillRevision() {
      RevisionService.uploadPhotos();
      DamagesService.uploadPhotos();
      var damageRef = RevisionService.pushDamages(DamagesService.savedDamages).getKey();
      damageId = damageRef;
      vm.revision.timestamp = timestamp;
      vm.revision.damages_ref = damageRef;
      vm.revision.user_ref = SessionService.getAuthData();
      vm.revision.shipping_co = vm.container.shipping_co;
      vm.revision.container_ref = vm.container.number;
      if(vm.container.shipping_co === 'gash') {
        vm.revision.parts = angular.fromJson(angular.toJson(vm.tir_parts));
      }
      DamagesService.uploadPhotos();
    }

    function resetFields() {
      DamagesService.savedDamages = [];
      vm.toggleAll('in');
      vm.toggleAll('in');
      vm.toggleAll('out');
      vm.toggleAll('out');
      vm.noGenSet();
    }

    function goToPreview() {
      var _damages = DamagesService.savedDamages;
      vm.currentRevision = angular.copy(vm.revision);
      vm.currentRevision.damages = setupDamages(_damages);
      vm.currentRevision.container = vm.container.number;
      vm.currentRevision.shipping_co = vm.container.shipping_co;
      vm.currentRevision.timestamp = timestamp;
      if(vm.container.shipping_co === 'gash') {
        vm.currentRevision.parts = angular.fromJson(angular.toJson(vm.tir_parts));
      }
      showPreviewModal();
    }

    function showPreviewModal() {
      showModal('app/revision/preview/preview.html')
        .then(function(modal) {
           vm.modulesModal = modal;
        });
    }

    function showModal() {
      var modal = $q.defer();

      $ionicModal.fromTemplateUrl(templateUrl, {
        scope: previewScope
      })
      .then(function (_modal) {
        modal.resolve(_modal);
        _modal.show();
      }); 

      return modal.promise;
    }

    function setupDamages(damagesArray) {
      var properties = Object.keys(damagesArray);
      var damages = [];
      angular.forEach(properties, function(property) {
        var part = property;
        angular.forEach(damagesArray[property], function(damage) {
          damages.push(formatDamageObject(partName(part), damage));
        });
      });
      return damages;
    }

    function formatDamageObject(partKey, damage) {
      return {
        'part': partKey,
        'name': damage.name
      };
    }
    
    function partName(partKey) {
      for(var position = 0; position < PARTS.length; position++) {
        if (PARTS[position].key === partKey) {
          return PARTS[position].name;
        }
      }
    }

    function saveRevision(){
      $ionicLoading.show({
        template: 'Procesando contenedor...'
      });
      fillRevision();
      RevisionService.pushRevision(vm.revision);
      RevisionService.getRevisionRef(vm.revision.timestamp)
        .then(setLastRevision)
        .then(function() {
          $ionicLoading.hide();
          resetFields();
          $ionicPopup.show({
            title: 'Revisión de contenedor',
            template: 'Registrada con éxito',
            cssClass: 'text-center',
            buttons: [
            {
              text: 'Imprimir',
              type: 'button-positive',
              onTap: function(e) {
                $state.go('report', {containerId: vm.container.number, damagesRef: damageId, revisionsRef: revisionId});
              }
            },
            {
              text: 'Ok',
              type: 'button-positive',
              onTap: function(e) {
                $state.go('mainMenu');
              }
            }]
          });
        });
    }

    function isNew(container) {
      if (angular.isDefined(container.number)) {
        container.last_revision_ref = vm.container.last_revision_ref;
        ContainerService.updateContainer(container);
        lastRevisiondDeferred.resolve();
      } else {
        ContainerService.pushContainer(vm.container);
        lastRevisiondDeferred.resolve();
      }
    }

    function setLastRevision(result) {
      vm.container.last_revision_ref = result[0].$id;
      revisionId = result[0].$id;
      ContainerService.getContainer(vm.container.number)
        .then(isNew);
      return lastRevisiondDeferred.promise;
    }

    function goToViewDamages(container_view) {
      DamagesService.setActualView(container_view, vm.container.shipping_co);
      vm.currentDamages = DamagesService.loadPreviousDamage();
      showModal('app/viewDamages/viewDamages.html', 'Daños')
        .then(function(modal) {
          vm.damagesModal = modal;
        });
    }

    function showModal(templateUrl, title) {
      var modal = $q.defer();

      $ionicModal.fromTemplateUrl(templateUrl, {
        title: title,
        scope: $scope
      })
      .then(function(_modal_) {
        modal.resolve(_modal_);
        _modal_.show();
      });

      return modal.promise;
    }

    function deleteDamage(damage) {
      var damageIndex = vm.currentDamages.indexOf(damage);
      vm.currentDamages.splice(damageIndex, 1);
      $scope.$broadcast('damageDeleted', damage);

    }

    function hasNewDamages(view) {
      return false;
    }

    function save() {
      DamagesService.saveDamages();
      vm.damagesModal.remove();
    }

    function exit() {
      DamagesService.clearCurrentDamages();
      vm.damagesModal.remove();
    }

    function toggleAll(direction) {
      if(direction === 'in') {
        angular.forEach(vm.tir_parts, function(part) {
          part.in = (vm.allInPartsChecked)? false : true;
        });
        vm.allInPartsChecked = !vm.allInPartsChecked;
      } else {
        angular.forEach(vm.tir_parts, function(part) {
          part.out = (vm.allOutPartsChecked)? false : true;
        });
        vm.allOutPartsChecked = !vm.allOutPartsChecked;
      }
    }

    function clearMoveReason() {
      vm.revision.move_reason = 'none';
    }

    function noGenSet() {
      vm.revision.battery = false;
      vm.revision.battery_seal = false;
      vm.revision.alternator = false;
      vm.revision.starter = false;
      vm.revision.electric_generator = false;
    }
  }
})();
