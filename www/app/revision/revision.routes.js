(function() {
  'use strict';

  angular
    .module('app.revision')
    .config(function($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('revision', {
          cache: false,
          url: '/revision',
          controller: 'RevisionController',
          controllerAs: 'vm',
          templateUrl: 'app/revision/revision.html'
        })
        .state('report', {
          cache: false,
          url: '/report/:containerId&:damagesRef&:revisionsRef',
          controller: 'ReportController',
          controllerAs: 'vm',
          templateUrl: 'app/revision/report/report.html'
        })
        .state('setting', {
          cache: false,
          url: '/setting/:containerId&:damagesRef&:revisionsRef',
          controller: 'SettingController',
          controllerAs: 'vm',
          templateUrl: 'app/revision/setting/setting.html'
        });
      $urlRouterProvider.otherwise('/');
    });
})();
