(function () {
  'use strict';

  angular
    .module('app.revision')
    .controller('SettingController', SettingController);

  /* @ngInject */
  function SettingController($state,
    $http,
    $interpolate,
    $scope,
    DamagesService,
    RevisionService,
    SessionService) {

    var vm = this;
    vm.saveChanges = saveChanges;

    activate();

    function activate() {
    }

    function saveChanges() {
      var ipAddress = vm.address;
      localStorage.setItem('ipAddress', ipAddress);
      $state.go('report', {
        containerId: $state.params.containerId,
        damagesRef: $state.params.damagesRef,
        revisionsRef: $state.params.revisionsRef});
    }

  }
})();
