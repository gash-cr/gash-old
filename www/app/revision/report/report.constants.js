(function() {
  'use strict';
  angular
  .module('app')
  .constant('GASH_DAMAGES', [
    {'abbreviation': 'C', 'name': 'Cortadura'},
    {'abbreviation': 'R', 'name': 'Rayón'},
    {'abbreviation': 'H', 'name': 'Hueco'},
    {'abbreviation': 'A', 'name': 'Abolladura'},
    {'abbreviation': 'Q', 'name': 'Quebradura'},
    {'abbreviation': 'F', 'name': 'Faltante'},
    {'abbreviation': 'P', 'name': 'Parche'},
    {'abbreviation': 'G', 'name': 'Gotera'}
  ])
  .constant('PARTS', [
    {'key': 'left', 'name': 'Lado Izquierdo'},
    {'key': 'inside', 'name': 'Interior'},
    {'key': 'right', 'name': 'Lado Derecho'},
    {'key': 'chassis_front', 'name': 'Chasis Frente'},
    {'key': 'chassis_up', 'name': 'Chasis Arriba'},
    {'key': 'chassis_back', 'name': 'Chasis Atrás'},
    {'key': 'chassis_side', 'name': 'Chasis Costado'},
    {'key': 'front', 'name': 'Frente'},
    {'key': 'roof', 'name': 'Techo'},
    {'key': 'back', 'name': 'Atrás'},
    {'key': 'floor', 'name': 'Piso'},
    {'key': 'left', 'name': 'Izquierda'},
    {'key': 'left', 'name': 'Izquierda'},
    {'key': 'left', 'name': 'Izquierda'},
  ]);

})();
