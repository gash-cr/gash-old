(function () {
  'use strict';

  angular
    .module('app.revision')
    .controller('ReportController', ReportController);

  /* @ngInject */
  function ReportController($state,
                            $scope,
                            $log,
                            DamagesService,
                            RevisionService,
                            SessionService,
                            LastRevisionService,
                            ObjectFormaterService,
                            PrintService,
                            DAMAGE_PARTS,
                            PARTS) {

    var reportScope = $scope.$new();
    var vm = $scope;
    reportScope.revision = {};

    vm.printReport = printReport;
    vm.printSettings = printSettings;
    vm.goToHome = goToHome;
    vm.sort = {
      property: 'name',
      reverse: false
    };
    vm.sortBy = sortBy;

    activate();

    function activate() {
      vm.container = $state.params.containerId;
      revisionReport();
    }

    function sortBy(propertyName) {
      vm.sort.reverse = (vm.sort.property === propertyName) ? !vm.sort.reverse : false;
      vm.sort.property = propertyName;
    }

    function revisionReport() {
      RevisionService.getRevision($state.params.revisionsRef)
        .then(getDamages);
    }

    function getDamages(lastRevision) {
      LastRevisionService.getDamages(lastRevision.damages_ref)
        .then(function(damages) {
          ObjectFormaterService.formatDamages(damages)
            .then(function(formatedDamages) {
              vm.revision = lastRevision;
              vm.revision.damages = setupDamages(formatedDamages);
              reportScope.revision = lastRevision;
              reportScope.revision.damages = setupDamages(formatedDamages);
              reportScope.revision.container = vm.container;
              reportScope.revision.user = SessionService.getAuthData();
            });
        });
    }

    function setLastRevision(lastRevision) {
      vm.revision = lastRevision;
      reportScope.revision = lastRevision;
      reportScope.revision.container = vm.container;
      reportScope.revision.user = SessionService.getAuthData();
    }

    function printReport() {
      var _address = localStorage.getItem('ipAddress');
      PrintService.printReport(reportScope.revision, _address)
        .catch(handlePrintReportError);
    }

    function handlePrintReportError(error) {
      $log.error(error);
    }

    function printSettings() {
      $state.go('setting', {
        containerId: $state.params.containerId,
        damagesRef: $state.params.damagesRef,
        revisionsRef: $state.params.revisionsRef
      });
    }

    function setupDamages(damagesArray) {
      var properties = Object.keys(damagesArray);
      var damages = [];
      angular.forEach(properties, function(property) {
        var part = property;
        angular.forEach(damagesArray[property], function(damage) {
          damages.push(formatDamageObject(partName(part), damage));
        });
      });
      return damages;
    }

    function formatDamageObject(partKey, damage) {
      return {
        'part': partKey,
        'name': damage.name
      };
    }

    function goToHome() {
      $state.go('mainMenu');
    }

    function partName(partKey) {
      for(var position = 0; position < PARTS.length; position++) {
        if (PARTS[position].key === partKey) {
          return PARTS[position].name;
        }
      }
    }

  }
})();
