(function() {
  'use strict';

  angular
    .module('app.mainMenu')
    .config(function($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('mainMenu', {
          url: '/main-menu',
          templateUrl: 'app/mainMenu/mainMenu.html',
          controller: 'MainMenuController',
          controllerAs: 'vm',
          cache: false
        });
      $urlRouterProvider.otherwise('/');
    });
})();
