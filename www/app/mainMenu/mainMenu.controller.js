(function() {
  'use strict';

  angular
    .module('app.mainMenu')
    .controller('MainMenuController', MainMenuController);

  /* @ngInject */

  function MainMenuController($state, $ionicPopup, DamagesService, LoginService, ContainerService, RevisionService) {
    var vm = this;
    vm.logOut = logOut;
    vm.getContainer = getContainer;
    vm.goToForm = goToForm;
    vm.setLastRevDate = setLastRevDate;

    activate();

    function activate() {
      vm.isLoaded = false;
      vm.containerExists = false;
    }

    function logOut() {
      LoginService.logOut();
    }

    function setContainerData(container) {
      if (container.number) {
        vm.containerExists = true;
        vm.container = container;
        setLastRevDate(vm.container.last_revision_ref);
        RevisionService.setContainer(container);
      } else {
        vm.containerExists = false;
        vm.container = {
          number: vm.containerNumber
        };
      }
      vm.isLoaded = true;
    }

    function getContainer() {
      if (vm.containerNumber) {
        ContainerService.getContainer(vm.containerNumber)
          .then(setContainerData)
      }else{
        $ionicPopup.alert({
          title: 'Error',
          template: 'Ingrese el código del contenedor.'
        });
      }
    }

    function setLastRevDate(revisionRef) {
      RevisionService.getRevision(revisionRef)
        .then(function(revision){
          vm.container.lastRevDate = revision.timestamp;
        });
    }

    function goToForm(shippingCo) {
      if(vm.containerExists){
        $state.go('revision');
      } else {
        DamagesService.savedDamages = [];
        vm.container.shipping_co = shippingCo;
        RevisionService.setContainer(vm.container);
        $state.go('revision');
        }
    }
  }
})();
