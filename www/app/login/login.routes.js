(function() {
  'use strict';

  angular
    .module('app.login')
    .config(function($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('login', {
          url: '/login',
          templateUrl: 'app/login/login.html',
          controller: 'LoginController',
          controllerAs: 'vm',
          cache: false
        });
      $urlRouterProvider.otherwise('/');
    });
})();
