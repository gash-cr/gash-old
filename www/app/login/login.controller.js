(function() {
  'use strict';

  angular
    .module('app.login')
    .controller('LoginController', LoginController);

  /* @ngInject */

  function LoginController(LoginService, SessionService, $ionicPopup, $state) {
    var vm = this;
    vm.authenticate = authenticate;

    activate();

    function activate() {
      vm.user = {};
    }

    function authenticate() {
        LoginService.logIn(vm.user.username)
          .then(handleLoginResponse)
          .catch(showErrorMessage);
    }

    function handleLoginResponse(user) {
      if (user.password === vm.user.password) {
        SessionService.setAuthData(user);
        $state.go('mainMenu');
      } else {
        showErrorMessage();
      }
    }

    function showErrorMessage() {
      $ionicPopup.alert({
        title: 'Error',
        template:'Usuario y/o contraseña incorrectos.'
      });
    }

  }
})();
