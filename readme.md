# Gash - Ionic App

### Installation
NPM and Bower need to be installed. The Node version used is 7.0.1

Install Ionic (working with version 3.2.0), Cordova (7.0.1) and ionic@legacy (2.2.3)

```shell
$ npm install -g ionic cordova ionic@legacy
```

Run bower and npm to get all dependencies needed.
```sh
$ npm install
$ bower install
```

When running `bower install` you may be given several options for the dependecies, the recomender selections are:
* `angular-ui-router`: option `2` - `angular-ui-router#0.3.0`
* `angular`: option `5` - `angular#>= 1.0.8`

###Runing on web browser (Chrome is recommended)
```sh
$ ionic serve
```
Wait until your browser is launched with the application. Use the *Responsive mode* (in Chrome: enter to *Inspect* and then use the *Device toolbar* [shortcut: `Ctrl + Shift + M`]) to visualize it correctly.

###Runing on Android device

In order to run the application on an Android device:

Add Android platform:
```sh
$ ionic platform add android
```

Connect the device to the computer, allow USB debugging and then execute:
```sh
$ ionic run android
```

###Generate an APK

First, install the  [Camera Plugin](https://github.com/asolano-pernix/cordova-plugin-wezka-nativecamera) (do it once)

```shell
$ cordova plugin add cordova-plugin-camera 
```

Now you can generate the APK

```shell
$ ionic build android
```

Usually, the APK will be saved at `platforms/android/build/outputs/apk/android-debug.apk`

###Setting up tests
In order to run Karma and protractor tests, please do the following:

Install Karma CLI globally
```sh
npm install -g karma-cli
```

Install protractor globally
```sh
npm install -g protractor
```

Use the web manager to install the chrome-driver and selenium server.
```sh
webdriver-manager update
```

To run protractor tests, execute:
```sh
ionic serve
webdriver-manager start  
protractor protractor.conf.js
```

## Styleguide

Refer to AngularJS John Papa Styleguide: https://github.com/johnpapa/angular-styleguide
