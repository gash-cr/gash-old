var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var inject = require('gulp-inject');
var jscs = require('gulp-jscs');

var paths = {
  sass: ['./scss/**/*.scss']
};

gulp.task('default', ['sass']);

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

// Fill index
gulp.task('build-index', function() {
  var target = gulp.src('www/index.html');
  var sources = gulp.src([
    'www/lib/angular-ui-router/release/angular-ui-router.min.js',
    'www/lib/firebase/firebase.js',
    'www/lib/angularfire/dist/angularfire.js',
    'www/lib/angular-messages/angular-messages.min.js',
    'www/lib/paper/dist/paper-core.min.js',
    'www/js/epos-print-5.0.0.js',
    'www/app/app.module.js',
    'www/app/app.*.js',
    'www/app/**/*.module.js',
    'www/app/**/*.js',
    'www/app/assets/styles/*.css'], {read: false});

  return target.pipe(inject(sources, {relative: true}))
    .pipe(gulp.dest('./www/'));

});

gulp.task('review', function() {
  return gulp.src(['./www/app/**/*.js', './www/tests/specs/**/*.js'])
    .pipe(jscs())
    .pipe(jscs.reporter());
});
